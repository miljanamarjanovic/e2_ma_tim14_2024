# Е2_MA_Тim14_2024



## Members:

- [Milena Marković](https://gitlab.com/MilenaM06),      RA 83/2020
- [Miljana Marjanović](https://gitlab.com/miljanamarjanovic),   RA 123/2020
- [Strahinja Praška](https://gitlab.com/strahinja803),     RA 245/2021
- [Anastasija Novaković](), RA 77/2020

## Getting started:

To build and run your app, follow these steps:  
Clone the repository.  
Open project in Android Studio.  
In the toolbar, select your app from the run configurations menu.  
In the target device menu, select the device that you want to run your app on.  
If you don't have any devices configured, you need to either create an Android Virtual Device to use the Android Emulator or connect a physical device.  
Click Run.  
Android Studio warns you if you attempt to launch your project to a device that has an error or a warning associated with it. Iconography and stylistic changes differentiate between errors (device selections that result in a broken configuration) and warnings (device selections that might result in unexpected behavior but are still runnable).  
